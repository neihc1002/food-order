namespace OrderFood.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.MenuFoods", "FoodId", "dbo.Foods");
            DropForeignKey("dbo.MenuFoods", "MenuId", "dbo.Menus");
            DropIndex("dbo.MenuFoods", new[] { "FoodId" });
            DropIndex("dbo.MenuFoods", new[] { "MenuId" });
            DropTable("dbo.MenuFoods");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.MenuFoods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FoodId = c.Guid(),
                        MenuId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.MenuFoods", "MenuId");
            CreateIndex("dbo.MenuFoods", "FoodId");
            AddForeignKey("dbo.MenuFoods", "MenuId", "dbo.Menus", "MenuId");
            AddForeignKey("dbo.MenuFoods", "FoodId", "dbo.Foods", "FoodId");
        }
    }
}
