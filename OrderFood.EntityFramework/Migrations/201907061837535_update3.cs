namespace OrderFood.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Status", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "DinerQuantity", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Orders", "DinerQuantity", c => c.Int(nullable: false));
            DropColumn("dbo.Orders", "Status");
        }
    }
}
