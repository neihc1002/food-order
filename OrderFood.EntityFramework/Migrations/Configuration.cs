using OrderFood.Models;

namespace OrderFood.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<OrderFood.EntityFramework.OrderAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(OrderFood.EntityFramework.OrderAppContext context)
        {
            context.Users.Add(new User()
            {
                Id = Guid.NewGuid(),
                Username = "admin",
                Password = "admin",
                Role = "admin",
                CreatedAt = DateTime.Now,
                Status = 1
            });
            context.SaveChanges();
        }
    }
}
