namespace OrderFood.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetails", "MenuId", "dbo.Menus");
            DropIndex("dbo.OrderDetails", new[] { "MenuId" });
            AddColumn("dbo.Foods", "Type", c => c.String());
            DropTable("dbo.Menus");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        MenuId = c.Guid(nullable: false),
                        Name = c.String(),
                        Status = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SalePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsSale = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.MenuId);
            
            DropColumn("dbo.Foods", "Type");
            CreateIndex("dbo.OrderDetails", "MenuId");
            AddForeignKey("dbo.OrderDetails", "MenuId", "dbo.Menus", "MenuId");
        }
    }
}
