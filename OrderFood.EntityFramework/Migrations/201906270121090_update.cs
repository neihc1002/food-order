namespace OrderFood.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Guid(nullable: false),
                        Name = c.String(),
                        Status = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Foods",
                c => new
                    {
                        FoodId = c.Guid(nullable: false),
                        Name = c.String(),
                        Image = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SalePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsSale = c.Boolean(nullable: false),
                        Details = c.String(),
                        Ingredient = c.String(),
                        Status = c.Int(nullable: false),
                        CategoryId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.FoodId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.MenuFoods",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FoodId = c.Guid(),
                        MenuId = c.Guid(),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Foods", t => t.FoodId)
                .ForeignKey("dbo.Menus", t => t.MenuId)
                .Index(t => t.FoodId)
                .Index(t => t.MenuId);
            
            CreateTable(
                "dbo.Menus",
                c => new
                    {
                        MenuId = c.Guid(nullable: false),
                        Name = c.String(),
                        Status = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SalePrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsSale = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.MenuId);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderId = c.Guid(),
                        FoodId = c.Guid(),
                        MenuId = c.Guid(),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Foods", t => t.FoodId)
                .ForeignKey("dbo.Menus", t => t.MenuId)
                .ForeignKey("dbo.Orders", t => t.OrderId)
                .Index(t => t.OrderId)
                .Index(t => t.FoodId)
                .Index(t => t.MenuId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderId = c.Guid(nullable: false),
                        CustomerName = c.String(),
                        DinerQuantity = c.Int(nullable: false),
                        Time = c.DateTime(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.OrderId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        OrderId = c.Guid(nullable: false),
                        Username = c.String(),
                        Password = c.String(),
                        Role = c.String(),
                        Status = c.Int(nullable: false),
                        CreatedAt = c.DateTime(),
                        ModifiedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.OrderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "MenuId", "dbo.Menus");
            DropForeignKey("dbo.OrderDetails", "FoodId", "dbo.Foods");
            DropForeignKey("dbo.MenuFoods", "MenuId", "dbo.Menus");
            DropForeignKey("dbo.MenuFoods", "FoodId", "dbo.Foods");
            DropForeignKey("dbo.Foods", "CategoryId", "dbo.Categories");
            DropIndex("dbo.OrderDetails", new[] { "MenuId" });
            DropIndex("dbo.OrderDetails", new[] { "FoodId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.MenuFoods", new[] { "MenuId" });
            DropIndex("dbo.MenuFoods", new[] { "FoodId" });
            DropIndex("dbo.Foods", new[] { "CategoryId" });
            DropTable("dbo.Users");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.Menus");
            DropTable("dbo.MenuFoods");
            DropTable("dbo.Foods");
            DropTable("dbo.Categories");
        }
    }
}
