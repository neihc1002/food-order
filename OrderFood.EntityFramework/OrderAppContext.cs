﻿using OrderFood.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.EntityFramework
{
    public class OrderAppContext: DbContext
    {
        public OrderAppContext() : base("Default")
        {
        }
        public virtual DbSet<Food> Foods { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<User> Users { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().Property(e => e.Id).HasColumnName("OrderId");
            modelBuilder.Entity<Order>().Property(e => e.Id).HasColumnName("OrderId");
            modelBuilder.Entity<Food>().Property(e => e.Id).HasColumnName("FoodId");
            modelBuilder.Entity<Category>().Property(e => e.Id).HasColumnName("CategoryId");
        }
    }
}
