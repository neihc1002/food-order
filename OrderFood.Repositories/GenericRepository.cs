﻿using OrderFood.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private OrderAppContext _context = null;
        private DbSet<T> table = null;
        public GenericRepository()
        {
            this._context = new OrderAppContext();
            table = _context.Set<T>();
        }
        public GenericRepository(OrderAppContext _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }
        public IQueryable<T> GetAll()
        {
            return table;
        }
        public T GetById(object id)
        {
            return table.Find(id);
        }
        public void Insert(T obj)
        {
            table.Add(obj);
        }
        public void Update(T obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(object id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
        }
    }
}
