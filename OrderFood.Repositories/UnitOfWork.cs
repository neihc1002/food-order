﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderFood.EntityFramework;

namespace OrderFood.Repositories
{
    public class UnitOfWork<T> : IUnitOfWork<T> where T : class
    {
        private OrderAppContext _context = new OrderAppContext();
        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public IGenericRepository<T> Repository()
        {
            return new GenericRepository<T>(_context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
