﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Repositories
{
    public interface IUnitOfWork<T> : IDisposable where T : class
    {
        IGenericRepository<T> Repository();
        void Save();
        Task SaveAsync();
    }
}
