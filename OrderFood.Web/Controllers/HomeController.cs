﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using OrderFood.EntityFramework;
using OrderFood.Models;
using OrderFood.Repositories;
using OrderFood.Web.Constants;
using OrderFood.Web.Models;

namespace OrderFood.Web.Controllers
{
    public class HomeController : Controller
    {
        private OrderAppContext _context;

        public HomeController()
        {
            _context = new OrderAppContext();
        }

        public async Task<ActionResult> Index()
        {
            HomeViewModel viewModel = new HomeViewModel();
            var category = _context.Categories.Where(c => c.Status == 1);
            var food = _context.Foods.Where(f => f.Status == 1 && f.Type.Equals("FOOD"));
            var foods =
                (from c in category
                    join f in food on c.Id equals f.CategoryId into temp
                    from first in temp.DefaultIfEmpty()
                    group first by c
                    into gr
                    select new {Key = gr.Key, Value = gr.ToList()});
            viewModel.Foods = await foods.ToDictionaryAsync(t => t.Key, t => t.Value);
            viewModel.Menus = await _context.Foods.Where(f => f.Type == FoodType.Menu && f.Status==1).ToListAsync();
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public async Task<JsonResult> UpdateCartAsync(CartInput input)
        {
            var food = await _context.Foods.FindAsync(input.Id);
            List<CartInput> list = new List<CartInput>();
            if (Session["cart"] != null)
            {
                list = (List<CartInput>) Session["cart"] as List<CartInput>;
                var exist = list.FirstOrDefault(c => c.Id == input.Id);

                if (exist != null)
                {
                    exist.Quantity = input.Quantity;
                }
                else
                {
                    input.Details = food.Details;
                    input.Name = food.Name;
                    input.Price = food.IsSale ? food.SalePrice : food.Price;
                    list.Add(input);
                }
            }
            else
            {
                input.Details = food.Details;
                input.Name = food.Name;
                input.Price = food.IsSale ? food.SalePrice : food.Price;
                list.Add(input);
            }

            Session["cart"] = list;
            return await Task.FromResult(Json(new {status = true, cart = list.Count}));
        }

        public PartialViewResult CartView()
        {
            var list = Session["cart"];
            if (list != null)
            {
                list = list as List<CartInput>;
            }
            else
            {
                list = new List<CartInput>();
            }

            return PartialView("Cart", list);
        }

        public PartialViewResult OrderView()
        {
            return PartialView("Order");
        }

        [HttpPost]
        public async Task<JsonResult> UpdateOrder(OrderInput input)
        {
            var list = (List<CartInput>)Session["cart"] as List<CartInput>;

            var order = new Order()
            {
                Price = list.Sum(t=>t.Price*t.Quantity).Value,
                Id = Guid.NewGuid(),
                CreatedAt = DateTime.Now,
                Email = input.Email,
                PhoneNumber = input.Phone,
                Time = input.Date,
                CustomerName = input.Name,
                DinerQuantity = input.Quantity,
                Status = 0
                
            };
            foreach (var item in list)
            {
                var orderDetails = new OrderDetail()
                {
                    Id = Guid.NewGuid(),
                    OrderId = order.Id,
                    Quantity = item.Quantity,
                    FoodId = item.Id,
                    Price = item.Price.Value,
                    CreatedAt = DateTime.Now
                };
                order.OrderDetails.Add(orderDetails);
            }

            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            Session.Clear();
            return Json(new {status = true});
        }

        public JsonResult Delete(Guid? id)
        {
           var list =  Session["cart"] as List<CartInput>;
           var item = list.FirstOrDefault(t => t.Id == id);
           list.Remove(item);
           return Json(new { },JsonRequestBehavior.AllowGet);
        }
    }
}