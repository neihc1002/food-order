﻿$(document).ready(function () {
    $("#food-open").on('click',
        function (e) {
            e.preventDefault();
            GetFoodTable("FOOD");
        });
    $("#menu-open").on('click',
        function (e) {
            e.preventDefault();
            GetFoodTable("MENU");
        });
    $("#category-open").on('click',
        function (e) {
            e.preventDefault();
            GetCategoryTable();
        });
    $("#order-open").on('click',
	    function (e) {
		    e.preventDefault();
		    GetOrderTable();
	    });
});

function GetFoodTable(type) {
    var link = '/Manage/Dashboard/GetFoodPage';
    $.ajax({
        url: link,
        type: "get", //send it through get method
        data: {
            type: type
        },
        success: function (res) {
            $("#content-main").html(res);
            $('#food-table').DataTable();
            InitFoodModal();
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
}

function GetCategoryTable() {

    $.get('/Manage/Dashboard/GetCategoryPage', function (res) {
        $("#content-main").html(res);
        $('#main-table').DataTable();
        InitCategoryModal();
    });
}

function GetOrderTable() {

    $.get('/Manage/Dashboard/GetOrderPage', function (res) {
		$("#content-main").html(res);
		$('#main-table').DataTable();
	});
}
function changeValue(data, name, value) {
    for (var i in data) {
        if (data[i].name === name) {
            data[i].value = value;
            break; //Stop this loop, we found it!
        }
    }
}

function InitFoodModal() {
    $('#add-food-btn').on("click",
        function () {
            console.log($(this).data("type"));
            OpenFoodModal($(this).data("type"));
        });
    $('.edit-food').on('click',
        function () {
            OpenFoodModal($(this).data("type"), $(this).data("id"));
        });
}

function InitCategoryModal() {
    $('#add-btn').on("click",
        function () {
            OpenCategoryModal();
        });
    $('.edit-btn').on('click',
        function () {
            OpenCategoryModal($(this).data("id"));
        });
}

function OpenFoodModal(type, id) {
    var link = '/Manage/Dashboard/CreateOrEditFoodModal';
    //if (id) {
    //    link += "?type=" + type + "&id=" + id;
    //}
    $.ajax({
        url: link,
        type: "get", //send it through get method
        data: {
            id: id,
            type: type
        },
        success: function (res) {
            $("#modal-append").html(res);
            $(".categories").selectpicker();
            $('#add-food').modal();
            $("#save-food-btn").on("click",
                function () {
                    var data = $('#add-food-form').serializeArray();
                    var isSale = $('input[name="IsSale"]:checked').length > 0;
                    var active = $('input[name="Active"]:checked').length > 0;
                    changeValue(data, "IsSale", isSale);
                    changeValue(data, "Active", active);
                    var formData = new FormData();
                    for (var i = 0; i < data.length; i++) {
                        formData.append(data[i].name, data[i].value);
                    }
                    var imagefile = document.getElementById("food-image").files[0];
                    formData.append("Image", imagefile);
                    $.ajax({
                        type: "POST",
                        url: '/Manage/Dashboard/UpdateFood',
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            $('#add-food').modal('toggle');
                            setTimeout(function () {
                                GetFoodTable(type);
                            }, 1000);
                        },
                        error: function (error) {
                        }
                    });

                });
        },
        error: function (xhr) {
            //Do Something to handle error
        }
    });
    //$.get(link, function (res) {
    //    $("#modal-append").html(res);
    //    $(".categories").selectpicker();
    //    $('#add-food').modal();
    //    $("#save-food-btn").on("click",
    //        function () {
    //            var data = $('#add-food-form').serializeArray();
    //            var isSale = $('input[name="IsSale"]:checked').length > 0;
    //            var active = $('input[name="Active"]:checked').length > 0;
    //            changeValue(data, "IsSale", isSale);
    //            changeValue(data, "Active", active);
    //            var formData = new FormData();
    //            for (var i = 0; i < data.length; i++) {
    //                formData.append(data[i].name, data[i].value);
    //            }
    //            var imagefile = document.getElementById("food-image").files[0];
    //            formData.append("Image", imagefile);
    //            $.ajax({
    //                type: "POST",
    //                url: '/Manage/Dashboard/UpdateFood',
    //                data: formData,
    //                dataType: 'json',
    //                contentType: false,
    //                processData: false,
    //                success: function (response) {
    //                    $('#add-food').modal('toggle');
    //                    setTimeout(function () {
    //                        GetFoodTable();
    //                    }, 1000);
    //                },
    //                error: function (error) {
    //                }
    //            });

    //        });
    //});
}

function OpenCategoryModal(id) {
    var link = '/Manage/Dashboard/CreateOrEditCategoryModal';
    if (id) {
        link += "?id=" + id;
    }
    $.get(link, function (res) {
        $("#modal-append").html(res);
        $('#add-modal').modal();
        $("#save-btn").on("click",
            function () {
                var data = $('#add-form').serializeArray();
                var active = $('input[name="Active"]:checked').length > 0;
                changeValue(data, "Active", active);
                var formData = new FormData();
                for (var i = 0; i < data.length; i++) {
                    formData.append(data[i].name, data[i].value);
                }
                $.ajax({
                    type: "POST",
                    url: '/Manage/Dashboard/UpdateCategory',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('#add-modal').modal('toggle');
                        setTimeout(function () {
                            GetCategoryTable();
                        }, 1000);
                    },
                    error: function (error) {
                    }
                });

            });
    });
}