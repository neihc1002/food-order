﻿$(document).ready(function () {
    $(".order-status").on("click",
        function (e) {
            var link = '/Manage/Dashboard/ChangeStatusOrder';
            $.ajax({
                url: link,
                type: "get", //send it through get method
                data: {
                    id: $(this).data("id"),
                    status: $(this).data("value")
                },
                success: function (res) {
	                GetOrderTable();
                },
                error: function (error) {
                }
            });

        });
    $('.details-item').on("click",
	    function(e) {
            e.preventDefault();
            var link = '/Manage/Dashboard/GetOrderDetails';
            $.ajax({
	            url: link,
	            type: "get", //send it through get method
	            data: {
		            id: $(this).data("id"),
	            },
	            success: function (res) {
                    $("#details-modal .modal-body").html(res);
                    $("#details-modal").modal();
	            },
	            error: function (error) {
	            }
            });
	    });

})