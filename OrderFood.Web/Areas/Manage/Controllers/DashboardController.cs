﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using OrderFood.EntityFramework;
using OrderFood.Models;
using OrderFood.Web.Areas.Manage.Models;
using OrderFood.Web.Constants;

namespace OrderFood.Web.Areas.Manage.Controllers
{
    public class DashboardController : Controller
    {
        private readonly OrderAppContext _context;

        public DashboardController()
        {
            _context = new OrderAppContext();
        }
        // GET: Manage/Dashboard
        public ActionResult Index()
        {


            return View();
        }

        public async Task<PartialViewResult> GetFoodPage(string type)
        {
            var model = await _context.Foods.Where(f => f.Type == type).Select(f => new FoodViewModel()
            {
                Id = f.Id,
                Image = f.Image,
                Ingredient = f.Ingredient,
                IsSale = f.IsSale,
                Name = f.Name,
                Price = f.Price,
                SalePrice = f.SalePrice,
                Type = f.Type,
                Details = f.Details
            }).ToListAsync();
            ViewBag.Type = type;
            return PartialView("FoodPage", model);
        }

        public async Task<PartialViewResult> GetCategoryPage()
        {
            var model = await _context.Categories.Where(c => c.Status != -1).Select(f => new CategoryViewModel()
            {
                Id = f.Id,
                Name = f.Name,
                Active = f.Status == 1 ? true : false
            }).ToListAsync();

            return PartialView("CategoryPage", model);
        }

        public async Task<PartialViewResult> GetOrderPage()
        {
            var model = await _context.Orders.Select(o => new OrderViewModel()
            {
                Id = o.Id,
                CustomerName = o.CustomerName,
                Sta = o.Status,
                Quantity = o.DinerQuantity,
                PhoneNumber = o.PhoneNumber,
                Email = o.Email,
                Time = o.Time
            }).ToListAsync();

            return PartialView("OrderPage", model);
        }

        public async Task<PartialViewResult> GetOrderDetails(Guid? id)
        {
            var items = await _context.OrderDetails.Where(o=>o.OrderId == id).Select(o => new OrderDetailsViewModel()
            {
                Name = o.Food.Name,
                Quantity = o.Quantity,
            }).ToListAsync();
            return PartialView("OrderDetails",items);
        }

        public async Task<JsonResult> ChangeStatusOrder(Guid? id, int status)
        {
            var item = await _context.Orders.FindAsync(id);
            item.Status = status;
            _context.Orders.AddOrUpdate(item);
            await _context.SaveChangesAsync();
            return Json(new {status=true },JsonRequestBehavior.AllowGet);
        }

        public async Task<PartialViewResult> CreateOrEditFoodModal(Guid? id, string type)
        {
            CreateOrEditViewModel model;
            if (id.HasValue)
            {
                var exist = await _context.Foods.FirstOrDefaultAsync(t => t.Id == id);
                model = new CreateOrEditViewModel()
                {
                    IsSale = exist.IsSale,
                    SalePrice = exist.SalePrice,
                    Id = exist.Id,
                    Price = exist.Price,
                    Name = exist.Name,
                    Ingredient = exist.Ingredient,
                    Active = exist.Status == 1 ? true : false,
                    Type = exist.Type,
                    Details = exist.Details,
                    Categories = await _context.Categories.Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString(), Selected = c.Id == exist.CategoryId }).ToListAsync()
                };
            }
            else
            {
                model = new CreateOrEditViewModel()
                {
                    Type = type,
                    Categories = await _context.Categories.Select(c => new SelectListItem() { Text = c.Name, Value = c.Id.ToString() }).ToListAsync()
                };
            }
            return PartialView("CreateOrEditFoodModal", model);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateFood(CreateOrEditFoodInput input)
        {
            Food food;
            string fileName = null;
            if (input.Image != null && input.Image.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/Upload/Image/"),
                        Path.GetFileName(input.Image.FileName));
                    input.Image.SaveAs(path);
                    fileName = input.Image.FileName;
                }
                catch (Exception ex)
                {
                }
            if (!input.Id.HasValue)
            {
                food = new Food()
                {
                    Id = Guid.NewGuid(),
                    IsSale = input.IsSale,
                    CategoryId = input.CategoryId,
                    Name = input.Name,
                    Ingredient = input.Ingredient,
                    Price = input.Price,
                    SalePrice = input.SalePrice,
                    Status = input.Active ? 1 : 0,
                    CreatedAt = DateTime.Now,
                    Image = fileName,
                    Type = input.Type,
                    Details = input.Details
                };
            }
            else
            {
                var exist = await _context.Foods.FindAsync(input.Id);
                exist.IsSale = input.IsSale;
                exist.CategoryId = input.CategoryId;
                exist.Name = input.Name;
                exist.Ingredient = input.Ingredient;
                exist.Price = input.Price;
                exist.SalePrice = input.SalePrice;
                exist.Status = input.Active ? 1 : 0;
                exist.ModifiedAt = DateTime.Now;
                exist.Image = fileName != null ? fileName : exist.Image;
                exist.Details = input.Details;
                exist.Type = input.Type;
                food = exist;
            }
            _context.Foods.AddOrUpdate(food);
            await _context.SaveChangesAsync();
            return Json(new { status = true });
        }

        public async Task<PartialViewResult> CreateOrEditCategoryModal(Guid? id)
        {
            CategoryViewModel model;
            if (id.HasValue)
            {
                var exist = await _context.Categories.FirstOrDefaultAsync(t => t.Id == id && t.Status != -1);
                model = new CategoryViewModel()
                {
                    Id = exist.Id,
                    Name = exist.Name,
                    Active = exist.Status == 1 ? true : false
                };
            }
            else
            {
                model = new CategoryViewModel();
            }
            return PartialView("CreateOrEditCategoryModal", model);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateCategory(CategoryViewModel input)
        {
            Category category;
            if (!input.Id.HasValue)
            {
                category = new Category()
                {
                    Id = Guid.NewGuid(),

                    Name = input.Name,
                    Status = input.Active ? 1 : 0,
                    CreatedAt = DateTime.Now,
                };
            }
            else
            {
                var exist = await _context.Categories.FindAsync(input.Id);
                exist.Name = input.Name;
                exist.Status = input.Active ? 1 : 0;
                exist.ModifiedAt = DateTime.Now;
                category = exist;
            }
            _context.Categories.AddOrUpdate(category);
            await _context.SaveChangesAsync();
            return Json(new { status = true });
        }
    }
}