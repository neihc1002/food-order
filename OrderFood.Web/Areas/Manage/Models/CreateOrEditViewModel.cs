﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderFood.Web.Areas.Manage.Models
{
    public class CreateOrEditViewModel
    {
        public Guid? Id  { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public bool IsSale { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public string Ingredient { get; set; }
        public string Details { get; set; }
        public string Type { get; set; }

        public bool Active { get; set; }
    }
}