﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFood.Web.Areas.Manage.Models
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
        public Guid? Id { get; set; }
        public bool Active { get; set; }
    }
}