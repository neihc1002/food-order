﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OrderFood.Web.Areas.Manage.Models
{
    public class OrderViewModel
    {
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Quantity { get; set; }
        public Guid? Id { get; set; }
        public int Sta { get; set; }
        public DateTime? Time { get; set; }

        public string Status
        {
            get
            {
                string status = "";
                switch (this.Sta)
                {
                    case 0:
                        status = "Pending";
                        break;
                    case 1:
                        status = "Accepted";
                        break;
                    case 2:
                        status = "Done";
                        break;
                    case 3:
                        status = "Rejected";
                        break;
                }
                return status;

            }
        }

        public List<SelectListItem> StatusItems()
        {
            List<SelectListItem> list = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text = "Accept",
                    Value = "1",
                },
                new SelectListItem()
                {
                    Text = "Reject",
                    Value = "3",
                },
                new SelectListItem()
                {
                    Text = "Finish",
                    Value = "2",
                }
            };
            if (this.Sta == 1)
            {
                list.RemoveAt(0);
                list.RemoveAt(0);
            }
            else if (this.Sta == 0)
            {
                list.RemoveAt(2);
            }
            else if (this.Sta == 2 || this.Sta == 3)
            {
                list = new List<SelectListItem>();
            }
            return list;
        }
    }
}