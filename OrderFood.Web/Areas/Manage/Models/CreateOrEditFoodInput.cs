﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFood.Web.Areas.Manage.Models
{
    public class CreateOrEditFoodInput
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public bool IsSale { get; set; }
        public bool Active { get; set; }
        public Guid? CategoryId { get; set; }
        public string Ingredient { get; set; }
        public string Details { get; set; }
        public string Type { get; set; }
        public HttpPostedFileBase Image { get; set; }
    }
}