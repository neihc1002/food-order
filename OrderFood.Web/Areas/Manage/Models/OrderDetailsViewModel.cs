﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFood.Web.Areas.Manage.Models
{
    public class OrderDetailsViewModel
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}