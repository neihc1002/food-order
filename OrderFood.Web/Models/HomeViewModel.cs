﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OrderFood.Models;

namespace OrderFood.Web.Models
{
    public class HomeViewModel
    {
        public Dictionary<Category,List<Food>> Foods { get; set; }
        public List<Food> Menus { get; set; }
    }
}