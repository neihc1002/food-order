﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFood.Web.Models
{
    public class OrderInput
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public string Quantity { get; set; }
        public string Email { get; set; }
    }
}