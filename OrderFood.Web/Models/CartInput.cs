﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OrderFood.Web.Models
{
    public class CartInput
    {
        public Guid? Id { get; set; }
        public int Quantity { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public string Details { get; set; }
    }
}