﻿$(document).ready(function() {
    $(function () {
        $('#book_date').datetimepicker();
    });

    $('#order-btn').on("click",
        function(e) {
            e.preventDefault();
            var data = $("#order-form").serialize();
            $.ajax({
                url: "/home/UpdateOrder",
                type: "post", //send it through get method
                data: data,
                success: function(res) {
                    $("#cart-modal").modal("toggle");
                },
                error: function(xhr) {
                    //Do Something   to handle error
                }
            });
        });
});