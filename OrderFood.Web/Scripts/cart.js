﻿$(document).ready(function () {

    $('.item-number').on('change',
        function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            $.post('home/UpdateCartAsync', {
                id: id,
                quantity: $(this).val()
            }, function (res) {
                var link = 'Home/CartView';
                $.ajax({
                    url: link,
                    type: "get", //send it through get method
                    success: function (res) {
                        $("#cart-modal .modal-body").html(res);
                    },
                    error: function (xhr) {
                        //Do Something to handle error
                    }
                });
            });
        });
    $("#order").on("click",
        function (e) {
            e.preventDefault();
            var link = 'Home/OrderView';
            $.ajax({
                url: link,
                type: "get", //send it through get method
                success: function (res) {
                    $("#cart-modal .modal-body").html(res);
                    //$("#cart-modal").modal();
                },
                error: function (xhr) {
                    //Do Something to handle error
                }
            });
        });
    $(".delete-cart").on("click",
        function (e) {
            e.preventDefault();
            var link = 'Home/Delete';
            $.ajax({
                url: link,
                type: "get", //send it through get method
                data: {
                    id: $(this).data("id")
                },
                success: function (res) {
                    var link = 'Home/CartView';
                    $.ajax({
                        url: link,
                        type: "get", //send it through get method
                        success: function (res) {
                            $("#cart-modal .modal-body").html(res);
                        },
                        error: function (xhr) {
                            //Do Something to handle error
                        }
                    });
                },
                error: function (xhr) {
                    //Do Something to handle error
                }
            });
        });
});