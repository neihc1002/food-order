﻿$(document).ready(function () {
    $(".add-cart").on("click",
        function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            $.post('/home/UpdateCartAsync', {
                id: id,
                quantity:1
            }, function (res) {
                $(".badge").text(res.cart);
                console.log(res);
            });
        });
    $("#cart").on("click",
        function(e) {
            e.preventDefault();
            var link = '/Home/CartView';
            $.ajax({
                url: link,
                type: "get", //send it through get method
                success: function (res) {
                    $("#cart-modal .modal-body").html(res);
                    $("#cart-modal").modal();
                },
                error: function (xhr) {
                    //Do Something to handle error
                }
            });
        });
});