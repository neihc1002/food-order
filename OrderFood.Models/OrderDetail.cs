﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Models
{
    public class OrderDetail : Entity
    {
        public Guid? OrderId { get; set; }
        public Guid? FoodId { get; set; }
        public Guid? MenuId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public virtual Order Order { get; set; }
        public virtual Food Food { get; set; }
    }
}
