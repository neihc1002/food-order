﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Models
{
    public class Food : Entity
    {
        public Food()
        {
            OrderDetails=new HashSet<OrderDetail>();
        }
        public string Name { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }
        public decimal SalePrice { get; set; }
        public bool IsSale { get; set; }
        public string Details { get; set; }
        public string Ingredient { get; set; }
        public int Status { get; set; }
        public string Type { get; set; }
        public Guid? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
