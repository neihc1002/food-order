﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Models
{
    public class Order : Entity
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }
        public string CustomerName { get; set; }
        public string DinerQuantity { get; set; }
        public DateTime? Time { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public decimal Price { get; set; }
        public int Status { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
