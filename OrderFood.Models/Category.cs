﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderFood.Models
{
    public class Category : Entity
    {
        public string Name { get; set; }
        public int Status { get; set; }
    }
}
